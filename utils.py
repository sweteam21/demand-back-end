import pymysql.cursors
import datetime

# Utils for runHTTPRequest.py
class HTTPRequestUtils:


    # Function Desc: Hashes a given password with salt using pbkdf2 hash alg and 16 byte salt
    # Pre: Parameter given is a String (password)
    # Post: Returns a String (hashed and salted representation of passed String)
    def hashPassword(plainPassword):
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        hashed = hashlib.pbkdf2_hmac('sha512', plainPassword.encode('utf-8'), salt, 100)

        return (salt + binascii.hexlify(hashed)).decode('ascii')


    # Function Description: Validates a given plain password against a given hashed and salted password.
    #                       Takes salt from given hashed pass, and hashes/salts given plain pass with found salt.
    #                       Compares newly hashed/salted pass with pre given hashed/salted pass.
    # Pre: Parameters given are both Strings (hashed/salted pass and plain pass)
    # Post: Returns True if plain password matches hashed and salted pass, False if not.
    def verifyPassword(storedPassword, passwordToValidate):
        salt = storedPassword[:64]
        toCompare = storedPassword[64:]
        print("HASHED: " + toCompare)
        hashed = hashlib.pbkdf2_hmac('sha512', passwordToValidate.encode('utf-8'), salt.encode('ascii'), 100)
        hashed = binascii.hexlify(hashed).decode('ascii')
        print("GIVEN: " + hashed)
        print("RESULT: " + str(hashed == toCompare))

        return hashed == toCompare


    # Program Description: insert new user into demand database in the users table
    # Pre: The passed in variables are all strings
    # Post: The new User record is successfully created and inserted into the demand database Users table, then the connection is closed
    def insertNewUser(self, firstName, lastName, email, username, password):
        # connect to the database
        connection = pymysql.connect(host='localhost', port = 6021,
                                     user='root', password='password', db='team21Demand',
                                     charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

        with connection.cursor() as cursor:
            # Insert user info into database
            sql = "INSERT INTO Users (user_firstname, user_lastname, user_email, user_username, user_password) VALUES (%s, %s, %s, %s, %s)"

            cursor.execute(sql, [firstName, lastName, email, username, password])

            # commit to save changes to database
            connection.commit()
            # close connection
            connection.close()


    # Program Description: Function returns the password associated with the passed in username found in the demand databases User table
    # Pre: The passed in variable is a string
    # post: The function returns a string of the password that is pulled from the database table
    def returnUsersPassword(self, username):
        # connect to the database
        connection = pymysql.connect(host='localhost', port = 6021, 
                                     user='root', password='password', db='team21Demand',
                                     charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
        with connection.cursor() as cursor:
            # read a single record
            sql = "SELECT user_password FROM Users WHERE user_username= %s"
            cursor.execute(sql, username)
            # return password
            result = cursor.fetchone()

            # close connection
        connection.close()
        return result['user_password']


        # Program Description: Function returns the id associated witht he passed in username found in the demand databases User table
        # Pre: The passed in value is a string
        # post: The function returns an int of the id that is pulled from the database table 
    def returnUsersId(self, username):
        # connect to the database
        connection = pymysql.connect(host='localhost', port=6021,
                                     user='root', password='password', db='team21Demand',
                                     charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

        with connection.cursor() as cursor:
            # read a single record
            sql = "SELECT user_id FROM Users WHERE user_username= %s"
            cursor.execute(sql, username)
            # return password
            result = cursor.fetchone()

            # close connection
            connection.close()
            return result['user_id']


    # Program Description: Create an order
    # Pre: passed in variables are of type string,int,string,string
    # post: The new Order record is successfully created and inserted into the demand database order table, then the connection is closed
    def createOrder(self, contents, userID, address, typeOfService):

        time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

        #Create a connection to Database
        connection = pymysql.connect(host='localhost', port = 6021,
                                     user='root', password='password', db='team21Demand',
                                     charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

        with connection.cursor() as cursor:

            # Insert user info into database
            sql = "INSERT INTO Orders (order_contents, user_id, order_address, order_tos, order_datetime) VALUES (%s, %s, %s, %s, %s)"

            cursor.execute(sql, [contents, userID, address, typeOfService, time])

            # commit to save changes to database
            connection.commit()

            # close connection
            connection.close()

if __name__ == "__main__":
    main()
