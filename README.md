**WeGo Demand Side Back End**
*Reed Gauthier, Kathryn Reck, Sergiy Ensary, Summer Klimko, Kyle Flett*

**About**
This repository contains the Python files used to connect to the demand side back end. 
This contains what the demand side common services pages uses for database requests, API calls, password hashing, etc.

**How to use this repository**
To use the .py files in this repository make sure that mysql and pymysql are installed.
team21-demand-HTTPRequestHandler.py is the main file that contains HTTP Request functions (GET, POST) but it also uses helper methods from utils.py.

**Next Steps**
The GET function in team21-demand-HTTPRequestHandler.py hasn't been finished yet as we havent needed to use it yet and have been focusing on other issues.
